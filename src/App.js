import "./App.css";
import Layout from "./components/Layout";
import Home from "./pages/home";
import About from "./pages/about";
import { Routes, Route } from "react-router-dom";
import Blog from "./pages/blog";
import NotFound from "./pages/notFound";
import Edit from "./pages/edit";

function App() {
  return (
    <div className="App">
      <Layout>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/blog">
            <Route index element={<Blog />} />
            <Route path="edit/:id" element={<Edit />} />
          </Route>
          <Route path="*" element={<NotFound />} />
        </Routes>
      </Layout>
      {/* <Routes>
        <Route path="*" element={<NotFound />} />
      </Routes> */}
    </div>
  );
}

export default App;
