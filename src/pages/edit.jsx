import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";

const Edit = () => {
  const { id } = useParams();

  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [alert, setAlert] = useState(false);

  //func to submit
  const submitBlog = (e) => {
    e.preventDefault();
    console.log(title);
    console.log(body);
    axios
      .post("https://jsonplaceholder.typicode.com/posts/", {
        title: title,
        body: body,
      })
      .then((res) => {
        console.log(res.data);
      });
    setAlert(true);
    setTitle("");
    setBody("");
  };

  useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((res) => {
        setTitle(res.data.title);
        setBody(res.data.body);
      });
  }, []);

  return (
    <>
      <div
        className={
          alert
            ? "m-8 bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
            : " hidden"
        }
        role="alert"
      >
        {/* <strong class="font-bold">Holy smokes</strong> */}
        <span className="block sm:inline">Blog is Updated!</span>
        <span className="absolute top-0 bottom-0 right-0 px-4 py-3">
          <svg
            className="fill-current h-6 w-6 text-green-500"
            role="button"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <title>Close</title>
            <path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
          </svg>
        </span>
      </div>
      <div className="p-4 m-5 border">
        <h2 className="mb-8 font-bold">Edit a Blog</h2>
        <form action="" name="form" method="get" onSubmit={submitBlog}>
          <div className="my-3">
            <label htmlFor="">Title</label>
            <input
              type="text"
              name="heading"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              className="border outline-none block w-1/2 my-2"
            />
          </div>
          <div className="my-3">
            <label htmlFor="">Body</label>
            <textarea
              type="text"
              name="body"
              value={body}
              onChange={(e) => setBody(e.target.value)}
              className="border outline-none block w-1/2 my-2"
            ></textarea>
          </div>
          <button
            type="submit"
            className="mt-5 bg-blue-600 text-white px-3 my-3"
          >
            Add
          </button>
        </form>
      </div>
    </>
  );
};

export default Edit;
