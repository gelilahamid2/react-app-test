import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const url = "https://jsonplaceholder.typicode.com/posts/";

const Home = () => {
  const [blogs, setBlogs] = useState();

  useEffect(() => {
    axios.get(url).then((res) => {
      setBlogs(res.data);
      // console.log(res.data);
    });
  }, []);

  return (
    <>
      <div className="m-4">
        <h3 className="font-bold">Blog</h3>
        <table className="mt-12">
          <thead>
            <tr>
              <th className="py-2 px-3 border-y capitalize font-bold">id</th>
              <th className="py-2 px-3 border-y capitalize font-bold">title</th>
              <th className="py-2 px-3 border-y capitalize font-bold">body</th>
              <th className="py-2 px-3 border-y capitalize font-bold"></th>
            </tr>
          </thead>
          <tbody>
            {blogs?.map((data, idx) => (
              <tr key={idx}>
                <td className="px-3 py-1 border-y">{data?.id}</td>
                <td className="px-3 py-1 border-y">
                  {data.title.slice(0, 15)}...
                </td>
                <td className="px-3 py-1 border-y">
                  {data.body.slice(0, 25)}...
                </td>
                <td className="px-3 py-1 border-y text-green-500">
                  <Link to={`/blog/edit/${data.id}`}>edit</Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Home;
