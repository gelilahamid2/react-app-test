import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";
import { BrowserRouter } from "react-router-dom";
// import {
//   ApolloClient,
//   InMemoryCache,
//   ApolloProvider,
//   gql,
// } from "@apollo/client";

// const client = new ApolloClient({
//   uri: "https://staging-gateway.lmis.gov.et/v1/graphql",
//   cache: new InMemoryCache(),
// });

// const client = ...

// client
//   .query({
//     query: gql`
//       query organization {
//         organization_namespace {
//           organization_organization {
//             id
//             name
//             description
//             logo
//           }
//         }
//       }
//     `,
//   })
//   .then((result) => console.log(result));

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);
