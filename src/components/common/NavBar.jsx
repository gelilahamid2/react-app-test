import React from "react";
import { Link } from "react-router-dom";

const NavBar = () => {
  return (
    <div className="p-3 border-b">
      <div className="flex justify-between">
        <div className="font-bold text-red-500">MoBlog</div>
        <ul className="flex flex-wrap justify-evenly">
          <li>
            <Link to="/" className="hover:text-red-400">
              Blog
            </Link>
          </li>
          <li className="px-2">
            <Link to="/blog" className="hover:text-red-400">
              Write
            </Link>
          </li>
          <li>
            <Link to="/about" className="hover:text-red-400">
              About
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default NavBar;
