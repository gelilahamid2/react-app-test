import React from "react";
import NavBar from "./common/NavBar";

const Layout = ({ name, children }) => {
  return (
    <>
      <div className="container mx-auto">
        {" "}
        <NavBar />
        {children}
        {/* <Footer /> */}
      </div>
    </>
  );
};

export default Layout;
